import { Component } from '@angular/core';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { RepositoriesService } from '../provider/repositories.service';
import _ from 'lodash';
import { ProfilePage } from '../profile/profile.page';
import { cJson } from '../utility/default-contribution';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  limit: number = 10;
  page: number = 1;
  total_count: number = 0;
  repositories: any[];
  constructor(private repoSvr: RepositoriesService, private toastCtrl: ToastController, private loadCtrl: LoadingController, private modalController: ModalController) {

  }

  ngOnInit() {
    this.reInitialize();
    this.loadRepositories();
  }

  doRefresh(event) {
    this.reInitialize();
    this.loadRepositories(event);
  }

  reInitialize() {
    this.limit = 10;
    this.page = 1;
    this.total_count = 0;
    this.repositories = [];
  }



  loadMore(event) {
    this.page++;
    this.loadRepositories(event);
  }
  async loadRepositories(event?) {
    let loading
    if (!event) {
      loading = await this.presentLoading("Loading..");
      await loading.present();
    }
    this.repoSvr.getrepositories(this.limit, this.page).then((res: any) => {
      if (loading) loading.dismiss();
      if (event) event.target.complete();
      if (res && !_.isEmpty(res.items)) {
        console.log(res);
        this.mapRecord(res)
      } else {
        this.presentToast("No record found.");
      }
    }).catch(err => {
      console.log(err)
      if (loading) loading.dismiss();
      if (event) event.target.complete();
      this.presentToast("Error in fetching repositories.");
    });
  }

  mapRecord(res) {
    let repo_constructors = [];
    this.total_count = res.total_count;
    this.repositories.push(...res.items);
    this.repositories.map(o => {
      o.loadContributor = true;
      // repo_constructors.push(this.repoSvr.getInfoByUrl(o.contributors_url));
      repo_constructors.push(Promise.resolve([]));
      o.ishideAndShowContributors = false;
      return o;
    })

    Promise.all(repo_constructors).then(cons => {
      if (!_.isEmpty(cons)) {
        cons.forEach((o, index) => {
          let r = this.repositories[index];
          if (r) {
            if (_.isEmpty(o)) r.contributors = _.cloneDeep(cJson);
            else r.contributors = o;
            r.loadContributor = false;
          } else {
            r.contributors = [];
            r.loadContributor = false;
          }
          if (r.contributors.length > 7) {
            r.backup_contributors = _.cloneDeep(r.contributors);
            let _c = r.contributors.splice(0, 7);
            r._contributors = r.contributors.splice(7, r.contributors.length);
            r.contributors = _c;
          }
        })
      }
    })
  }

  hideAndShowContributors(repo, value) {
    repo.ishideAndShowContributors = value;
    if (value) {
      repo.contributors = repo.backup_contributors;
    } else {
      repo.backup_contributors = _.cloneDeep(repo.contributors);
      let _c = repo.contributors.splice(0, 7);
      repo._contributors = repo.contributors.splice(7, repo.contributors.length);
      repo.contributors = _c;
    }
  }
  async presentLoading(msg) {
    const loading = await this.loadCtrl.create({
      message: msg,
    });

    return loading;
  }


  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  openProfile(contributors) {
    this.presentModal(ProfilePage, contributors);
  }

  async presentModal(ModalPage, data: any) {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        data: data
      },
      cssClass: 'user-madal'
    });
    return await modal.present();
  }

  openCustomTab(url) {
    console.log(url);
  }
}
