import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  @Input() data: any;
  user:any;
  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() { 
    this.user=this.data;
  }
  async closeModel() {
    await this.modalController.dismiss();
  }

}
