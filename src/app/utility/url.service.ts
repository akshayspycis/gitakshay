import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
      localGOKServer: string;
      backendUrlObserver: Subject<any> = new BehaviorSubject(null);
      private _apiEndpoints: Array<{ name: string, uri: string }>;
      PLATFORM_API_HOST:string="https://api.github.com";
      constructor() {
          this._apiEndpoints = [
              { name: "search", uri: "/search/repositories" },
          ];
      }
  
  
      getURL(functioname: string, params?: any) {
          return this.PLATFORM_API_HOST + this.getURI(functioname, params);
      }
  
      getURI(functioname: string, params?: any) {
          let _self = this;
          let uri;
          let api = _self._apiEndpoints.find(api => api.name == functioname);
          if (api) {
              uri = api.uri;
              for (let p in params) {
                  if (p && params[p]) {
                      uri = uri.replace(':' + p, params[p])
                  }
              }
          }
          return uri;
      }
  }
  