import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { UrlService } from '../utility/url.service';

@Injectable({
  providedIn: 'root'
})
export class RepositoriesService {

  constructor(private httpClient: HttpClient, private endpoint: UrlService) {

  }
  private handleError<T>(result?) {
    return (error: any): Observable<T[]> => {
      console.error(error);
      return of([]);
    };
  }

  getrepositories(limit, page): Promise<any[]> {
    return this.httpClient.get<any[]>(this.endpoint.getURL("search") + "?q=stars:>1"+"&per_page="+limit+"&page="+page)
      .pipe(
        tap(_ => console.log(`repository fetched`)),
        catchError(this.handleError<any[]>(`Error in fetching repository`))
      ).toPromise();
  }

  getInfoByUrl(url): Promise<any[]> {
    return this.httpClient.get<any[]>(url)
      .pipe(
        tap(_ => console.log(`information fetched`)),
        catchError(this.handleError<any[]>(`Error in fetching information`))
      ).toPromise();
  }
}
